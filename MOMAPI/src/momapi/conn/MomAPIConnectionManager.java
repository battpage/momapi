package momapi.conn;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;

import org.apache.log4j.Logger;

import com.ams.core.ui.FormFactory;
import com.ams.core.util.DomainObject;
import com.cgi.cml.common.momapi.client.MomentumInterfaceProperties;
import com.cgi.cml.common.momapi.xml.QueryBean;
import com.cgi.cml.common.momapi.xml.QueryResponseBean;
import com.cgi.cml.common.momapi.xml.QueryRowBean;
import com.cgi.cml.common.momapi.xml.QueryTermBean;

import com.cgi.cml.common.util.momapi.GenericServiceUtil;

public class MomAPIConnectionManager {
	private static MomAPIConnection momapi = null;
	private static Properties momAPIProperties = null;
	private static final String MOMAPI_PROP_FILE_NAME = "config/momapi.properties";
	private static Logger log = Logger.getLogger(MomAPIConnectionManager.class);
	
	public static Properties getMomAPIProperties() {
		
		if (momAPIProperties == null) {
			System.out.println("momAPIProperties is null");
			momAPIProperties = new Properties();
			File propertiesFile = new File(MOMAPI_PROP_FILE_NAME);
			try {
				momAPIProperties.load(new FileInputStream(propertiesFile));
			} catch (IOException e) {
				
				 log.debug("getMomAPIProperties()");
				e.printStackTrace();
			}
		}
		
		
		    System.setProperty("ams.appserver.ip", "10.16.80.53"); 
			System.setProperty("ams.appserver.port", "8211"); 
			System.setProperty("ams.appserver.user", "system"); 
			System.setProperty("ams.appserver.password", "weblogic1"); 
		    System.setProperty("java.security.auth.login.config", "c:/BASIS/test1/momentum/domain/JAAS.policy");
		    System.setProperty("csf.preferences.url","file:///dev\\basis\\deploy\\lib\\system\\enterprise\\csf.properties");
		    System.setProperty("weblogic.security.SSL.trustedCAKeyStore","C:/Oracle/Middleware/wlserver_10.3/server/lib/cacerts");
		    System.setProperty("weblogic.security.SSL.nojce","true");
		    System.setProperty("weblogic.ssl.JSSEEnabled","true");
		    System.setProperty("weblogic.ProductionModeEnabled","true");
		    System.setProperty("weblogic.Name", "AdminServer");
		    System.setProperty("changeSessionIdOnAuthentication", "false");
		    System.setProperty("ams.appserverutil","weblogic");    
		    System.setProperty("ams.deploypath","C:\\dev\\basis\\deploy");
	        System.setProperty("changeSessionIdOnAuthentication","false");
	        System.setProperty("ams.basis.dtdpath","C:\\BASIS\test1\\dtd_t1");
	        System.setProperty("ams.basis.metadatapath", "C:\\dev\\basis\\deploy");
	        System.setProperty("ams.basis.registrypath", "C:\\dev\\basis\\deploy\\basis\\xml\\app\\registry");
	        
	
		return momAPIProperties;
	
	}

	public static void setMomAPIProperties(Properties momAPIProperties) {
		
		MomAPIConnectionManager.momAPIProperties = momAPIProperties;
	}
	public static MomAPIConnection getMomAPIConnection() {
		if (momapi==null) {
			
			momapi=new MomAPIConnection(momAPIProperties);
//			if (momapi == null) {
//				System.out.println("still null");
//			}
//			else {
//				//System.out.println("momapi to string: " + momapi.toString());
//			
//				List<QueryTermBean> queryTerms = new ArrayList<QueryTermBean>();
//				List<String> listStr = new ArrayList<String>();
//				listStr.add(0, "USER_ID");
//				queryTerms.add(null);
//				 QueryBean queryBean = new QueryBean("TEST_AMS_SCTY_USER2", queryTerms, listStr);
//				 System.out.println("getTermGroupCount: " + queryBean.getTermGroupCount());
//				 System.out.println("getResultLimit: " + queryBean.getResultLimit());
//				 System.out.println("queryBean.toString() ***" + queryBean.toString() + "***");
//				 System.out.println("getClassId(): " + queryBean.getClassId());
//				 
//				 
//				 QueryResponseBean queryResponseBean = null;
//
//			        try {
//			            queryResponseBean = GenericServiceUtil.query(queryBean);
//			        }
//			        catch (Exception ex) {
//			            System.out.println("Error retrieving Facts  Atribute Definition" + ex);
//			           // Log.logException(CMLConstants.MOMAPI, this, "Error retrieving ApprovalTemplate", ex);
//			        }
//			        DomainObject result = null;
//
//			        if (queryResponseBean != null && queryResponseBean.getRowCount() > 0) {
//			        	System.out.println("in if statement");
//			            QueryRowBean row = queryResponseBean.getRow(0);
//			            result = (DomainObject)FormFactory.getInstance().newObject("TEST_AMS_SCTY_USER2");
//			            result.setAll(row.getAll());
//			        }
//			    //    System.out.println("resulot.toString(): " + result.toString());
//			}
		}
		return momapi;
	}
	
	public static void disconnect() {
		if (momapi != null) {
			momapi.disconnect();
			momapi = null;
		}
		
	}
	
	
	public static void main(String [] args) {
		MomAPIConnectionManager.getMomAPIProperties();
		//Properties props = System.getProperties();
		MomAPIConnectionManager.getMomAPIConnection();
		disconnect();
	}
}
