package momapi.conn;

import com.cgi.cml.common.momapi.client.MomentumInterfaceBean;
import com.cgi.cml.common.momapi.client.MomentumInterfaceBeanFactory;
import java.util.Properties;
import org.apache.log4j.Logger;
import com.ams.core.xml.XMLFactory;
import com.ams.core.xml.XMLReader;
import com.cgi.cml.common.momapi.xml.IdentityBean;
import com.cgi.cml.common.momapi.xml.ObjectBean;
import com.cgi.cml.common.momapi.xml.QueryBean;
import com.cgi.cml.common.momapi.xml.QueryResponseBean;


public class MomAPIConnection {
	private MomentumInterfaceBean momapi;
	private static Logger log = Logger.getLogger(MomAPIConnection.class);
	 protected MomAPIConnection(Properties momapiProperties) { 
		 if (log.isInfoEnabled()) {
			 log.info("Entered MomAPIConnection()");
		 }
		momapi = MomentumInterfaceBeanFactory.newInstance(momapiProperties);
		 
		 try {
			 XMLFactory.initialize(new XMLReader(), null);
		 }
		 catch (Exception ex) {
			 System.out.println("Error initallizing XML factory: " + ex.getMessage());
		 }
		 if (log.isDebugEnabled()) {
			 log.debug("Opening MomAPIConnection()");
		 }
	 }
	 public QueryResponseBean query(QueryBean queryBean) {
		 
		 return momapi.query(queryBean);
	 }
	 public void change(ObjectBean objectBean) {
		 objectBean = momapi.perform11(objectBean, null, "change");
	 }
	 public  ObjectBean select(IdentityBean idBean) {
	       ObjectBean objectBean = momapi.select(idBean, "Select", true);
	       return objectBean;
	 }
	 public void disconnect() {
		 if (momapi != null) {
			 momapi.disconnect();
		 }
	 }
}
